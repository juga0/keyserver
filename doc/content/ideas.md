+++
date = "2018-02-14T09:53:39Z"
title = "ideas"
draft = true

+++

# OpenPGP key server ideas

HKP/SKS server +:

- interface that allow to obtain updates on:
 - new subkeys
 - new user ids
 - new self-signatures
 - new third party certifications
- in a privacy manner, using Valodim's bloom filter ideas ([https://pad.stratum0.org/p/bloom-keyservers])


Roadmap draft:

- just hkp/hkps read-only support for a static keyring add import/merge via hkp/hkps
- bloom-filter
- sks synchronization
