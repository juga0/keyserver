+++
title = "soa"
draft = true
date = "2018-02-14T09:56:02Z"

+++

# Implementations out there

## HKP/SKS

### sks-keyserver


[sks-keyserver code](https://bitbucket.org/skskeyserver/sks-keyserver)

Wrotten in Ocaml

> Features
>
> :   Highly efficient and reliable reconciliation algorithm Follows
>     RFC2440 and RFC4880 carefully --- SKS supports new and old style
>     packets, photoID packets, multiple subkeys, and pretty much
>     everything allowed by the RFCs, including Elliptic Curve Public
>     keys (ECDH, ECDSA) found in RFC6637. Fully compatible with PKS
>     system --- can both send and receive syncs from PKS servers,
>     ensuring seamless connectivity. Simple configuration: each host
>     just needs a (partial) list of the other participating
>     key servers. Gossip is used to distribute information without
>     putting a heavy load an any one host. Supports HKP/web-based
>     querying, and machine readable indices
>

Cons:

 -   vulnerable to slowloris attack
 -   not multi-threaded

### hockeypuck

[hockeypuck](https://hockeypuck.github.io//)

Wrotten in Go

PostgreSQL 9.4+ backend (JSONB)

[DB scheme?](https://github.com/hockeypuck/pghkp/blob/v1/storage.go#L55)

Is it working/production?

### sks-rs

[sks-rs code](https://github.com/srct/sks-rs)

Wrotten in Rust using Rocket

Uses PostgreSQL and web framework

Doesn't seem finished.

## Rust libraries

OpenPGP:

[sequoia code](https://gitlab.com/sequoia-pgp/sequoia)

Web frameworks:

-   [Rocket](https://rocket.rs/)
-   [Actix-web](https://actix.github.io/actix-web)

ORM frameworks:

-   [Diesel](http://diesel.rs/)
