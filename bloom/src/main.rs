extern crate bloom_filter;
use bloom_filter::BloomFilter;

fn main() {
    // Create new BloomFilter with 1000 expected elements, and false positive rate of 3%
    let mut bf = BloomFilter::new(1000, 0.03);
}
