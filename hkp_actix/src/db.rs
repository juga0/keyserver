//! Db executor actor
use uuid;
use diesel;
use actix_web::*;
use actix::prelude::*;
use diesel::prelude::*;

use models;
use schema;

/// This is db executor actor. We are going to run 3 of them in parallel.
pub struct DbExecutor(pub SqliteConnection);

/// This is only message that this actor can handle, but it is easy to extend number of
/// messages.
pub struct CreateKey {
    pub fingerprint: String,
}

impl ResponseType for CreateKey {
    type Item = models::Key;
    type Error = Error;
}

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

impl Handler<CreateKey> for DbExecutor {
    type Result = MessageResult<CreateKey>;

    fn handle(&mut self, msg: CreateKey, _: &mut Self::Context) -> Self::Result {
        use self::schema::keys::dsl::*;

        let uuid = format!("{}", uuid::Uuid::new_v4());
        let new_key = models::NewKey {
            id: &uuid,
            fingerprint: &msg.fingerprint,
        };

        diesel::insert_into(keys)
            .values(&new_key)
            .execute(&self.0)
            .expect("Error inserting key");

        let mut items = keys
            .filter(id.eq(&uuid))
            .load::<models::Key>(&self.0)
            .expect("Error loading key");

        Ok(items.pop().unwrap())
    }
}
