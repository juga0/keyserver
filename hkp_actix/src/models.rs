use super::schema::keys;

#[derive(Serialize, Queryable)]
pub struct Key {
    pub id: String,
    pub fingerprint: String,
}

#[derive(Insertable)]
#[table_keys = "keys"]
pub struct NewKey<'a> {
    pub id: &'a str,
    pub fingerprint: &'a str,
}
